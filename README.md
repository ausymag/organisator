# organisator

Ein Framework für Organisationen, die eine Website anbieten und Teammitglieder einbinden wollen. Die Funktionalität ist in allen Teilen bewusst minimalistisch gehalten.

Die Umsetzung ist ohne irgend eine Form von "Meta-Programmierung" in RubyOnRails Standard-Mechanismen angelegt. Das Frontend ist ohne Einbezug von weiteren Bibliotheken in HTML/CSS ausgeführt. Die UI Gestaltung ist bewusst einfach und nüchtern gehalten und kann je nach Bedürfnissen der Organisation anders gestaltet und erweitert werden.

organisator bietet bisher (jeweils optional nach Bedürfnissen):

* [Mini CMS](/doc/01_mini_cms.md) für dynamische Webseiten mit selbst definierbarer Inhaltshierarchie
* Terminkalender mit Ereignissen, zu denen Gäste sich auch Corona-konform registrieren können.
* Blog mit Artikeln, die von verschiedenen Teammitgliedern verfasst und gegenseitig freigegeben werden können.
* [Profile für Teammitglieder](/doc/02_accounts.md], so dass diese auf der Website mit ihren eigenen Beschreibungen vorgestellt werden können.
* Newsletter Erstellung und Versende-Mechanik vom eigenen Server ohne externe Dienstleister.
* Kontakte Datenbank, die E-Mail Adressen von Interessent:innen aufnimmt, um dorthin einen Newsletter versenden zu können.
* Mitglieder Datenbank, um z.B. Vereins- oder Parteimitglieder aufzunehmen und Beitragszahlungen zu verfolgen.
* Interne Entscheidungsfindung mit [Systemischem Konsensieren]

organisator wird zur Zeit eingesetzt als Basis folgender Websites:

* [Gemeinwohllobby](https://gemeinwohllobby.de)
* [Ludwigsklima](https://ludwigsklima.de)
* [Solaroffensive](https://solaroffensive.org)
* [KlimalisteBW](https://klimaliste-bw.de)

## Bildschirmfotos

| Startseite von organisator |   |
| -------------------------- | - |
| ![Startseite von organisator - Computer](/doc/img/01_root.png) | ![Startseite von organisator - Mobil](/doc/img/01m_root.png) |

| Blog Seite von organisator |   |
| -------------------------- | - |
| ![Blog Seite von organisator - Computer](/doc/img/02_blog.png) | ![Blog Seite von organisator - Mobil](/doc/img/02m_blog.png) |

| Termine Seite von organisator |   |
| -------------------------- | - |
| ![Termine Seite von organisator - Computer](/doc/img/03_events.png) | ![Termine Seite von organisator - Mobil](/doc/img/03m_events.png) |

| Kontakt Formular von organisator |   |
| -------------------------- | - |
| ![Kontakt Formular von organisator - Computer](/doc/img/04_contacts.png) | ![Kontakt Formular von organisator - Mobil](/doc/img/04m_contacts.png) |

| Team Seite von organisator |   |
| -------------------------- | - |
| ![Team Seite von organisator - Computer](/doc/img/05_team.png) | ![Team Seite von organisator - Mobil](/doc/img/05m_team.png) |

| Mitgliederverwaltung von organisator (Admin Ansicht) |   |
| -------------------------- | - |
| ![Mitglieder Seite von organisator - Computer](/doc/img/06_memberships.png) | ![Mitglieder Seite von organisator - Mobil](/doc/img/06m_memberships.png) |

| Entscheidungsfindung mit Systemischem Konsensieren |   |
| -------------------------- | - |
| ![Entscheidungen Seite von organisator - Computer](/doc/img/08_decisions.png) | ![Entscheidungen Seite von organisator - Mobil](/doc/img/08m_decisions.png) |

| Login Seite von organisator |   |
| -------------------------- | - |
| ![Login Seite von organisator - Computer](/doc/img/07_login.png) | ![Login Seite von organisator - Mobil](/doc/img/07m_login.png) |

