module DecisionsHelper
  
  def h_decision_editable?
    logger.debug "> h_editable? @ DecisionsHelper"
    return false if @decision.voting? or @decision.done?
    current_account and
        current_account == @decision.account
  end
  
end
