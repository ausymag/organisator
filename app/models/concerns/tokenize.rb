module Tokenize
  extend ActiveSupport::Concern
  
  def fill_in_token
    # will generate one out of 53 ^ 5 tokens
    self.token ||= generate_token 5 if self.respond_to? :token
  end
  
  # --- INSTANCE METHODS -----------------------
  
  def generate_token( in_length = 10, with_chars = false )
    result = ''
    
    # erstes Zeichen aus "sicheren" Zeichen
    random_index = rand( TOKEN_LETTERS.size )
    result += TOKEN_LETTERS[ random_index ]
    
    ( in_length - 1 ).times do
      if with_chars
        random_index = rand( TOKEN_CHARS.size )
        result += TOKEN_CHARS[ random_index ]
      else
        random_index = rand( TOKEN_LETTERS.size )
        result += TOKEN_LETTERS[ random_index ]
      end
    end
    
    return result
  end
  
  def id_hash( in_supplement = '', in_length = 10 )
    digest = Digest::SHA1.new
    digest << id.to_s
    digest << token
    digest << in_supplement
    digest << created_at.strftime( '%m%d%H%M%S' )
    
    result = digest.hexdigest[0..(in_length - 1)]
    logger.debug "-- id_hash( '#{in_supplement}' ):#{result}"
    return result
  end
  
  # --- CONSTANTS ------------------------------
  
  # 52 unique identifiable letters
  TOKEN_LETTERS = %w( 2 3 4 5 6 7 8 9 a b c d e f h i j k m n p q r s t u v w x y z A B C D E F H K L M N P Q R S T U V W X Y Z )
  
  # 18 unique characters
  TOKEN_CHARS = TOKEN_LETTERS.dup | %w( ! $ % & / = ? + * # < > : ) << '"' << '§' << '(' << ')' << '.'
  
end
