class Contact < ApplicationRecord
  include Checkerize
  include Tokenize
  after_initialize :fill_in_token
  
  has_one :account, dependent: :destroy
  has_one :membership, dependent: :destroy
  has_many :mailings, dependent: :destroy
  has_many :received_messages, as: :receiver, class_name: 'Message', dependent: :destroy
  
  has_many :registrations, dependent: :destroy
  has_many :questions, dependent: :nullify
  has_many :answers, dependent: :nullify
  
  has_many :donations, dependent: :nullify
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        uniqueness: true,
        on: :create
  validate :check_question, on: :create
  
  validates :token, :email, :first_name, :last_name,
        presence: true
  validates_acceptance_of :read_privacy
  
  # --- SCOPES ---------------------------------
  
  scope :fresh, -> { where status: 'new' }
  scope :confirmed, -> {
        where status: 'confirmed' }
  scope :wants_news, -> {
        confirmed
        .where wants_news: true }
  scope :offers_help, -> {
        where offers_help: true }
  scope :new_since, -> (date) {
        where "created_at > ?", date }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def confirmed?
    status == 'confirmed'
  end
  
  def name_text
    [ first_name, last_name ].join ' '
  end
  
  def street_nr_text
    [ street, housenr ].compact.join ' '
  end
  
  def plz_city_text
    [ plz, city ].compact.join ' '
  end
  
  def secret
    id_hash [ first_name, last_name ].join
  end
  
  def quali_email
    '"' + name_text + '" <' + email + '>'
  end
  
  # --- PREDICATES -----------------------------
  
  def is_member?
    !membership.blank? and membership.member?
  end
  
  def is_accepted?
    !membership.blank? and membership.accepted?
  end
  
  def is_applicant?
    !membership.blank? and membership.application?
  end
  
  def has_account?
    !account.blank?
  end
  
  def show_to?( in_status = 'public' )
    return true if [ 'admin' ].include? in_status
    published?
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.new_since_days( in_days )
    self.new_since( Time.zone.now - in_days.days ).count
  end
  
  def self.increase_since_days( in_days )
    actual_days = self.
        new_since( Time.zone.now - in_days.days ).
        count
    prev_days = self.
        new_since( Time.zone.now - ( in_days * 2).days ).
        count - actual_days
    return ( actual_days * 100.0 / prev_days ).round - 100
  end
  
  def self.t_status_select
    STATUS_OPTIONS.collect { |o| [ self.human_attribute_name( "status.#{o}" ), o ] }
  end
  
private
  
  # --- CLASS METHODS --------------------------
  
  STATUS_OPTIONS = [
    'new',
    'order',
    'membership',
    'confirmed'
  ]
  
end

