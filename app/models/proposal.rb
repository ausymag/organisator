class Proposal < ApplicationRecord
  include Tokenize
  after_initialize :fill_in_token
  
  include Publish
  after_initialize :set_publish_status
  
  belongs_to :account
  belongs_to :decision
  has_many :votes, dependent: :destroy
  
  # --- VALIDATIONS ----------------------------
  
  validates_presence_of :title
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> {
        where status: 'published' }
  
  # --- INSTANCE METHODS -----------------------
  
  def iid
    "pr_#{token}"
  end
  
  def to_param
    "#{token}"
  end
  
  def position
    index = decision.proposals.find_index { |p| p == self }
    return index + 1
  end
  
  def has_votes?
    votes.published.count > 0
  end
  
  def voters_published
    unless defined? @voters_published
      the_votes = votes.includes( :account ).all.references( :account)
      @voters_published = the_votes.select { |v|
          v.published? }.collect { |v|
          v.account }
    end
    return @voters_published
  end
  
  def votes_for( in_account )
    unless defined? @_votes
      @_votes = votes.includes( :account ).where( account: in_account ).references( :account )
    end
    return @_votes
  end
  
  def vote_for( in_account )
    votes_for( in_account ).first
  end
  
  def vote_value_for( in_account )
    vote = vote_for( in_account )
    return nil if vote.blank?
    return vote.value
  end
  
  def votes_value
    unless defined? @votes_value
      if voters_published.count < 1
        @votes_value = 0
        
      else
        value_sum = votes.published.inject(0) { |sum, v|
          sum + v.value }
        @votes_value = ( value_sum * 100.0 / voters_published.count ).round / 100.0
      end
    end
    return @votes_value
  end
  
  def selection_value
    unless defined? @selection_value
      unless has_votes? or decision.voters_published.count == 0
        @selection_value = 0
        
      else
        @selection_value = votes.published.inject(0) { |sum, v|
          sum + v.value }
      end
    end
    return @selection_value
  end
  
end
