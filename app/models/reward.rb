class Reward < ApplicationRecord
  include Tokenize
  include Picturize
  
  after_initialize :fill_in_token
  
  belongs_to :funding, optional: true
  has_one_attached :picture
  
  has_many :donations, dependent: :nullify # TODO!!
  has_many :orderquants, dependent: :nullify # TODO!!
  
  # --- VALIDATIONS ----------------------------
  
  validates :token,
        presence: true,
        uniqueness: true
  validates_presence_of :title
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> { where( status: 'published' )
        .where( 'published_at < ?', Time.zone.now ) }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    "#{token}-#{title.parameterize}"
  end
  
  def preview_text
    title
  end
  
  def ordered
    donations.received.size +
        orderquants.inject( 0 ) { |sum, o|
            sum + o.quantity.to_i }
  end
  
  # --- Predicates -----------------------------
  
  def has_picture?
    picture.attached?
  end
  
  # --- CONSTANTS ------------------------------
  
  STATUS_OPTIONS = [
    'new',
    'published'
  ]
  
end
