class Mailing < ApplicationRecord
  belongs_to :contact
  belongs_to :packet, polymorphic: true
  
  # --- SCOPES ---------------------------------
  
  scope :ready, -> {
        where( status: 'ready' )
        .order 'send_at' }
  scope :to_send, -> {
        ready
        .where( [ 'send_at < ?', Time.zone.now ] )
        .order 'send_at' }
  scope :sent, -> {
        where( status: 'done' )
        .order 'updated_at DESC' }
  scope :last_month, -> {
        where( "updated_at > ?",
              Time.zone.now - 1.month )
        .order 'updated_at DESC' }
  
  # --- PREDICATES -----------------------------
  
  def sent?
    status == 'done'
  end
  
  # --- INSTANCE METHODS -----------------------
  
  def send_out
    if status == 'ready'
      logger.debug "> versenden @ mailing"
      self.update_attribute :status, 'sending'
      packet.send_out self
      
      self.update_attribute :status, 'done'
    end
  end
  
  def token
    result = Digest::SHA1.new
    result << id.to_s
    result << contact_id.to_s
    result << packet_id.to_s
    result << packet_type.to_s
    result << created_at.strftime( '%m%d%H%M%S' )
    return result.hexdigest[0..15]
  end
  
end
