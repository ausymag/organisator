class ProposalsController < ApplicationController
  before_action :set_proposal, only: [ :vote, :show, :edit, :update, :destroy ]
  before_action :set_decision, only: [ :create ]
  skip_before_action :require_team
  before_action :require_decider
  
  # --- CUSTOM METHODS -------------------------
  
  # POST / proposal/1/vote.js
  def vote
    return unless current_account and current_account.can_vote_for? @proposal
    
    @vote = @proposal.votes.for_account( current_account ).first
    if @vote
      if is_same_value? or is_selection_and_zero?
        @vote.destroy
      else
        @vote.value = params[ :value ].to_i
      end
      
    else
      return unless params[ :value ].to_i >= 0
      return if too_many_selections?
      
      @vote = @proposal.votes.build( status: 'new', account: current_account, value: params[ :value ].to_i )
    end
    logger.debug "-- vote:#{@vote.to_yaml}"
    @vote.save unless @vote.frozen?
  end
  
  # GET /proposals
  # GET /proposals.json
  def index
    render_404
    #@proposals = Proposal.all
  end
  
  # GET /proposals/1
  # GET /proposals/1.json
  def show
    redirect_to @proposal.decision
  end
  
  # GET /proposals/new
  def new
    redirect_to decisions_path, alert: 'Vorschlag kann nur zu einer Entscheidung eingebracht werden' and return unless params[ :decision_id ]
    
    @decision = Decision.find_by token: params[ :decision_id ]
    @proposal = @decision.proposals.build
  end
  
  # GET /proposals/1/edit
  def edit
  end
  
  # POST /proposals
  # POST /proposals.json
  def create
    @proposal = @decision.proposals.build( proposal_params )
    @proposal.status = 'published'
    @proposal.account = current_account
    
    respond_to do |format|
      if @proposal.save
        format.js { @decision.proposals.reload }
        format.json { render :show, status: :created, location: @proposal }
      else
        format.html { render :new }
        format.json { render json: @proposal.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /proposals/1
  # PATCH/PUT /proposals/1.json
  def update
    respond_to do |format|
      if @proposal.update(proposal_params)
        format.html { redirect_to @proposal, notice: 'Proposal was successfully updated.' }
        format.json { render :show, status: :ok, location: @proposal }
      else
        format.html { render :edit }
        format.json { render json: @proposal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /proposals/1
  # DELETE /proposals/1.json
  def destroy
    @decision = @proposal.decision
    @proposal.destroy
    
    respond_to do |format|
      format.html { redirect_to @decision, notice: 'Vorschlag wurde gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_proposal
    @proposal = Proposal.find_by token: params[ :id ]
    render_404 and return if @proposal.blank?
  end
  
  def set_decision
    @decision = Decision.find_by token: params[ :decision_id ]
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def proposal_params
    params.require( :proposal ).permit( :title, :description )
  end
  
  # --- PREDICATES -----------------------------
  
  def is_same_value?
    params[ :value ].to_i == @vote.value
  end
  
  def is_selection_and_zero?
    @proposal.decision.is_selection? and params[ :value ].to_i == 0
  end
  
  def too_many_selections?
    decision = @proposal.decision
    return ( decision.is_selection? and decision.vote_count_for( current_account ) >= decision.vote_count )
  end
  
end
