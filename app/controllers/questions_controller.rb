class QuestionsController < ApplicationController
  before_action :set_question, only: [ :show, :edit, :update, :destroy ]
  before_action :set_funding, only: [ :new, :create ]
  
  skip_before_action :require_team
  before_action :require_editor, except: [ :index, :show ]
  
  # --------------------------------------------
  # CRUD methods
  
  # GET /questions/1
  # GET /questions/1.js
  def show
    redirect_to @question.container and return
  end
  
  # GET /questions/new
  def new
    @question = @funding.questions.build status: 'new'
  end
  
  # GET /questions/1/edit
  def edit
  end
  
  # POST /questions
  # POST /questions.json
  def create
    @question = @funding.questions.build( question_params  )
    @question.status = 'new'
    @question.category = 'project'
    @question.contact = current_account.contact
    @question.openness = 'closed'
    
    respond_to do |format|
      if @question.save
        format.html { redirect_to @question.container, notice: 'Frage wurde erfolgreich gespeichert.' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update( question_params )
        format.html { redirect_to @question.funding, notice: 'Frage wurde erfolgreich geändert.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @container = @question.container
    @question.destroy
    
    respond_to do |format|
      format.html { redirect_to @container, notice: 'Frage wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_question
    @question = Question.find_by token: params[ :id ].split( '-' ).first
    render_404 and return if @question.blank?
  end
  
  def set_funding
    redirect_to fundings_path, alert: 'Frage kann nur zu einer Finanzierung erstellt werden.' and return unless params[ :funding_id ]
    
    if numeric? params[ :funding_id ]
      @funding = Funding.find params[ :funding_id ].to_i
    else
      @funding = Funding.find_by token: params[ :funding_id ].split( '-' ).first
    end
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def question_params
    params.require( :question ).permit( :status, :content )
  end
  
end
