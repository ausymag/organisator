class MembershipsController < ApplicationController
  before_action :require_admin, except: [ :activate, :new, :create ]
  skip_before_action :require_team, only: [ :activate, :new, :create ]
  
  before_action :check_enabled
  before_action :set_membership, only: [ :accept, :activate, :show, :edit, :update, :destroy ]
  
  # --- CUSTOM METHODS -------------------------
  
  # GET /memberships/1/accept
  def accept
    @membership.status = 'accepted'
    @membership.starts_at = Time.zone.now
    
    if @membership.save
      MembershipsMailer.with( membership: @membership ).
            membership_accepted.
            deliver_later
      render :show
      
    else
      redirect_to edit_membership_path( @membership ), notice: 'Du musst den Antrag erst korrekt ausfüllen und speichern'
    end
  end
  
  # GET /memberships/1/activate
  def activate
    @successful = false
    
    if @membership.secret == params[ :ring ]
      @membership.status = 'member'
      @membership.contact.status = 'confirmed'
      @membership.starts_at = Time.zone.now
      
      @successful = @membership.contact.save and @membership.save
    end
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /memberships
  # GET /memberships.json
  def index
    letter_selected = params[ :letter ]
    @letters = Membership.find_by_sql "
        select left( c.last_name, 1 ) as letter
          from memberships as m
            left join contacts as c
            on m.contact_id = c.id
          group by letter
          order by letter asc"
    
    @dates_list = params[ :letter ].to_s.length > 1
    case params[ :letter ]
      when 'unconfirmed'
        @memberships = Membership.unconfirmed.
            order :starts_at
      when 'new'
        @memberships = Membership.applications.
            order :starts_at
      when 'accepted'
        @memberships = Membership.accepted.
            order :starts_at
      when 'member'
        @memberships = Membership.member.
            order 'starts_at DESC'
      else
        @memberships = Membership.find_by_sql [ "
            select m.*
              from memberships as m
                left join contacts as c
                on m.contact_id = c.id
              where left( c.last_name, 1 ) = ?
              order by c.last_name", letter_selected ]
    end
  end
  
  # GET /memberships/1
  # GET /memberships/1.json
  def show
  end
  
  # GET /memberships/new
  def new
    contact = Contact.new
    contact = current_account.contact if current_account
    @membership = Membership.new( contact: contact )
  end
  
  # GET /memberships/1/edit
  def edit
  end
  
  # POST /memberships
  # POST /memberships.json
  def create
    @membership = Membership.new( membership_params )
    @membership.status = 'new'
    
    if current_account
      @membership.contact = current_account.contact
    else
      @membership.contact.status = 'membership'
      @membership.contact.wants_news = true
    end
    
    @membership.starts_at = Time.zone.now if @membership.starts_at.blank?
    
    respond_to do |format|
      if @membership.save
        session[ :membership_token ] = @membership.token
        
        format.html { render :create }
        format.json { render :show, status: :created, location: @membership }
      else
        format.html { render :new }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /memberships/1
  # PATCH/PUT /memberships/1.json
  def update
    respond_to do |format|
      if @membership.update( membership_params )
        format.html { redirect_to @membership, notice: 'Membership was successfully updated.' }
        format.json { render :show, status: :ok, location: @membership }
      else
        format.html { render :edit }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /memberships/1
  # DELETE /memberships/1.json
  def destroy
    # save exit date for once and log
    @membership.update_attribute :ends_at, Time.zone.now
    write_log( 'destroy membership', @membership.to_yaml, @membership )
    
    @contact = @membership.contact
    @membership.destroy
    respond_to do |format|
      format.html { redirect_to @contact, notice: 'Mitgliedschaft wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  
  # Use callbacks to share common setup or constraints between actions.
  def check_enabled
    render :not_enabled and return unless h_app_feature? :allow_membership
  end
  
  def set_membership
    @membership = Membership.find_by token: params[ :id ]
    render_404 and return if @membership.blank?
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def membership_params
    params.require( :membership ).permit( :gender, :birth_on, :street, :housenr, :plz, :city, :other_memberships, :fee_amount, :fee_period, :fee_mode, :account_owner, :iban, :accepts_debit, :recommendation, :phone_nr, :accepts_rules, :wants_membership, :starts_at, :ends_at, :last_payment_at, :contact_attributes => [ :email, :first_name, :last_name ] )
  end
  
end

