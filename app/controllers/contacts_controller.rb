class ContactsController < ApplicationController
  include ContactHelper
  
  skip_before_action :require_team, only: [ :activate, :unsubscribe, :show, :new, :edit, :create, :update ]
  before_action :set_contact, only: [ :activate, :unsubscribe, :show, :edit, :update, :destroy ]
  
  # GET /contacts/1/activate
  def activate
    @successful = false
    
    if @contact.secret == params[ :ring ]
      @contact.status = 'confirmed'
      
      if @contact.account.blank?
        @successful = @contact.save
        
      else
        @contact.account.status = 'confirmed'
        @successful = @contact.account.save and @contact.save
      end
    end
  end
  
  # GET /contacts/1/unsubscribe
  def unsubscribe
    redirect_to( root_path, alert: 'Ungültiger Link!' ) and return if @contact.id_hash != params[ :ato ]
    
    session[ :contact_destroy_id ] = @contact.id
  end
  
  # --- STANDARD CRUD METHODS ------------------
  
  # GET /contacts
  # GET /contacts.json
  def index
    @letter_selected = params[ :letter ]
    
    @letters = Contact.find_by_sql "
        select left( last_name, 1 ) as letter
          from contacts
          group by letter
          order by letter asc"
    case
      when @letter_selected.blank?
        @contacts = Contact.order( created_at: :desc ).limit 25
      when @letter_selected == '*'
        @contacts = Contact.
            where( "length( last_name ) = 0", @letter_selected ).
            order :last_name
      else
        @contacts = Contact.
            where( "left( last_name, 1 ) = ?", @letter_selected ).
            order :last_name
    end
  end
  
  # GET /contacts/1
  # GET /contacts/1.json
  def show
    unless h_is_admin?
      redirect_to @contact.account and return unless @contact.account.blank?
      redirect_to root_path, alert: 'Kein Zugriff, sorry!' and return
    end
  end
  
  # GET /contacts/new
  def new
    @contact = Contact.new wants_news: true
    @contact.seed_checker_question
  end
  
  # GET /contacts/1/edit
  def edit
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_contact_editable?( @contact )
  end
  
  # POST /contacts
  # POST /contacts.json
  def create
    @contact = Contact.new( contact_params )
    @contact.status = 'new'
    
    respond_to do |format|
      if @contact.save
        ContactsMailer.with( contact: @contact ).
            confirmation_mail.
            deliver_later
        
        format.html { render :registered }
        format.json { render :show, status: :created, location: @contact }
      else
        @contact.seed_checker_question
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    redirect_to login_path,
            notice: 'Bitte anmelden!' and return unless h_contact_editable?( @contact )
    
    respond_to do |format|
      if @contact.update(contact_params)
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    redirect_to @contact, alert: 'Kontakt hat immer noch Mitgliedschaft oder Account!' and return if @contact.membership or @contact.account
    letter = @contact.last_name[ 0 ]
    write_log( 'destroy contact', @contact.to_yaml, @contact )
    
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_path( letter: letter ), notice: 'Kontakt wurde erfolgreich gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_contact
    @contact = Contact.find_by token: params[ :id ]
    render_404 and return if @contact.blank?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require( :contact ).permit( :status, :email, :first_name, :last_name, :plz, :phone_nr, :read_privacy, :wants_news, :offers_help, :wants_form, :street, :housenr, :city, :remarks, :op_a, :op_b, :solved )
  end
  
end
