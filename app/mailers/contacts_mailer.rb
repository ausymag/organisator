class ContactsMailer < ApplicationMailer
  helper ApplicationHelper
  helper NewslettersHelper
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contacts_mailer.confirmation_mail.subject
  #
  def confirmation_mail
    @contact = params[ :contact ]
    @confirmation_link  = activate_contact_url( @contact, ring: @contact.secret )
    
    mail to: @contact.quali_email,
          subject: 'Bitte bestätigen... | ' + h_orga_name
  end
  
  def newsletter_mail( in_mailing )
    @mailing = in_mailing
    @newsletter = @mailing.packet
    @contact = @mailing.contact
    
    @name_text = @contact.name_text
    @content = @newsletter.content
    @contentparts = @newsletter.contentparts
    @storno_url = unsubscribe_contact_url( @contact, ato: @contact.id_hash )
    
    mail to: @contact.quali_email,
          subject: "Newsletter vom #{I18n.l @newsletter.send_at, format: :very_short_day} | " + h_orga_name
  end
  
  def registration_mail
    @registration = params[ :registration ]
    @contact = @registration.contact
    @event = @registration.event
    @confirmation_link  = activate_registration_url( @registration, ring: @registration.secret )
    
    mail to: @contact.quali_email,
          subject: 'Bitte bestätigen... | ' + h_orga_name
  end
  
  def message_mail
    @message = params[ :message ]
    @subject = @message.subject
    @subject = nil if @subject.blank?
    @receiver = @message.receiver
    @content = @message.content
    @sender = @message.sender
    
    mail to: @message.receiver.quali_email,
          subject: ( @subject || 'Eine Nachricht für dich!' ) + ' | ' + h_orga_name,
          reply_to: 'lisa@' + h_orga_domain
  end
  
end
