rails g scaffold Contact token status email first_name last_name read_privacy:boolean wants_news:boolean news_last_sent:datetime

rails g scaffold Event token status titel starts_at:datetime ends_at:datetime description location details:text url

rails g scaffold Article token status titel published_at:datetime description

rails g scaffold Account status login password_digest contact:references

rails g scaffold Contentpart token status kind position:integer container:references{polymorphic} content:text

rails g scaffold Page status parent:references{to_table: :pages} path title menu description

rails g scaffold Registration token status event:references contact:references plz city emergency_nr participants:text accepts_storage:boolean

rails g scaffold Membership token status contact:references gender birth_on:date street housenr plz city other_memberships:text fee_amount:decimal fee_period fee_mode wants_membership:boolean starts_at:datetime ends_at:datetime last_payment_at:datetime

rails g mailer Memberships membership_accepted

rails g scaffold Message sender:references{polymorphic} receiver:references{polymorphic} content:text read_at:datetime

rails g scaffold Decision token status title account:references description:text end_proposals_at:datetime end_voting_at:datetime

rails g scaffold Proposal token status account:references decision:references title description:text

rails g scaffold Argument token status account:references proposal:references title description:text

rails g resource Comment token status account:references entity:references{polymorphic} content:text

rails g resource Vote status account:references proposal:references value:integer

rails g resource Payment status membership:references amount:decimal paid_at:datetime

rails g model Log ip actor:references{polymorphic} action comment:text entity:references{polymorphic} user_agent

rails g resource Funding token status title account:references slogan short_text:text goal_1:integer goal_2:integer goal_3:integer goal_1_use goal_2_use goal_3_use starts_at:datetime ends_at:datetime performance_period description:text category location url some_1 some_2 some_3

rails g model Owner account:references funding:references

rails g resource Reward token status funding:references price:decimal title available:integer description:text delivery

rails g resource Question token status category container:references{polymorphic} contact:references openness content:text

rails g resource Answer token status question:references contact:references openness content:text

rails g resource Donation token status funding: references amount:decimal contact:references reward:references

rails g scaffold Order token status contact:references customer_kind customer_location delivery_org delivery_name delivery_street delivery_housenr delivery_plz delivery_city delivery_country invoice_org invoice_name invoice_street invoice_housenr invoice_plz invoice_city invoice_country read_privacy:boolean read_terms:boolean delivery_status delivery_date_at:datetime delivery_reference invoice_status invoice_date_at:datetime invoice_reference

rails g resource Orderquant order:references reward:references quantity:integer

