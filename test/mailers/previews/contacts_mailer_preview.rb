# Preview all emails at http://localhost:3000/rails/mailers/contacts_mailer
class ContactsMailerPreview < ActionMailer::Preview
  
  # Preview this email at http://localhost:3000/rails/mailers/contacts_mailer/confirmation_mail
  def confirmation_mail
    ContactsMailer.with( contact: Contact.last ).confirmation_mail
  end
  
  # Preview this email at http://localhost:3000/rails/mailers/contacts_mailer/confirmation_mail
  def newsletter_mail
    ContactsMailer.newsletter_mail( Mailing.last )
  end
  
  # Preview this email at http://localhost:3000/rails/mailers/contacts_mailer/registration_mail
  def registration_mail
    ContactsMailer.with( registration: Registration.first ).registration_mail
  end
  
  # Preview this email at http://localhost:3000/rails/mailers/contacts_mailer/message_mail
  def message_mail
    ContactsMailer.with( message: Message.last ).message_mail
  end
  
end
