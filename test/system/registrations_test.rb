require "application_system_test_case"

class RegistrationsTest < ApplicationSystemTestCase
  setup do
    @registration = registrations(:one)
  end

  test "visiting the index" do
    visit registrations_url
    assert_selector "h1", text: "Registrations"
  end

  test "creating a Registration" do
    visit registrations_url
    click_on "New Registration"

    check "Accepts storage" if @registration.accepts_storage
    fill_in "City", with: @registration.city
    fill_in "Contact", with: @registration.contact_id
    fill_in "Emergency nr", with: @registration.emergency_nr
    fill_in "Event", with: @registration.event_id
    fill_in "Participants", with: @registration.participants
    fill_in "Plz", with: @registration.plz
    fill_in "Status", with: @registration.status
    fill_in "Token", with: @registration.token
    click_on "Create Registration"

    assert_text "Registration was successfully created"
    click_on "Back"
  end

  test "updating a Registration" do
    visit registrations_url
    click_on "Edit", match: :first

    check "Accepts storage" if @registration.accepts_storage
    fill_in "City", with: @registration.city
    fill_in "Contact", with: @registration.contact_id
    fill_in "Emergency nr", with: @registration.emergency_nr
    fill_in "Event", with: @registration.event_id
    fill_in "Participants", with: @registration.participants
    fill_in "Plz", with: @registration.plz
    fill_in "Status", with: @registration.status
    fill_in "Token", with: @registration.token
    click_on "Update Registration"

    assert_text "Registration was successfully updated"
    click_on "Back"
  end

  test "destroying a Registration" do
    visit registrations_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Registration was successfully destroyed"
  end
end
