require "application_system_test_case"

class MembershipsTest < ApplicationSystemTestCase
  setup do
    @membership = memberships(:one)
  end

  test "visiting the index" do
    visit memberships_url
    assert_selector "h1", text: "Memberships"
  end

  test "creating a Membership" do
    visit memberships_url
    click_on "New Membership"

    fill_in "Birth on", with: @membership.birth_on
    fill_in "City", with: @membership.city
    fill_in "Contact", with: @membership.contact_id
    fill_in "Ends at", with: @membership.ends_at
    fill_in "Fee amount", with: @membership.fee_amount
    fill_in "Fee mode", with: @membership.fee_mode
    fill_in "Fee period", with: @membership.fee_period
    fill_in "Gender", with: @membership.gender
    fill_in "Housenr", with: @membership.housenr
    fill_in "Last payment at", with: @membership.fee_check_at
    fill_in "Other memberships", with: @membership.other_memberships
    fill_in "Plz", with: @membership.plz
    fill_in "Starts at", with: @membership.starts_at
    fill_in "Status", with: @membership.status
    fill_in "Street", with: @membership.street
    fill_in "Token", with: @membership.token
    check "Wants membership" if @membership.wants_membership
    click_on "Create Membership"

    assert_text "Membership was successfully created"
    click_on "Back"
  end

  test "updating a Membership" do
    visit memberships_url
    click_on "Edit", match: :first

    fill_in "Birth on", with: @membership.birth_on
    fill_in "City", with: @membership.city
    fill_in "Contact", with: @membership.contact_id
    fill_in "Ends at", with: @membership.ends_at
    fill_in "Fee amount", with: @membership.fee_amount
    fill_in "Fee mode", with: @membership.fee_mode
    fill_in "Fee period", with: @membership.fee_period
    fill_in "Gender", with: @membership.gender
    fill_in "Housenr", with: @membership.housenr
    fill_in "Last payment at", with: @membership.fee_check_at
    fill_in "Other memberships", with: @membership.other_memberships
    fill_in "Plz", with: @membership.plz
    fill_in "Starts at", with: @membership.starts_at
    fill_in "Status", with: @membership.status
    fill_in "Street", with: @membership.street
    fill_in "Token", with: @membership.token
    check "Wants membership" if @membership.wants_membership
    click_on "Update Membership"

    assert_text "Membership was successfully updated"
    click_on "Back"
  end

  test "destroying a Membership" do
    visit memberships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Membership was successfully destroyed"
  end
end
