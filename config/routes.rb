Rails.application.routes.draw do
  
  resources :orderquants
  
  resources :orders do
    member do
      get :activate
    end
  end
  
  resources :donations do
    member do
      put :advance
    end
  end
  
  resources :answers
  
  resources :questions do
    resources :answers
  end
  
  resources :rewards do
    resources :donations
  end
  
  resources :fundings do
    resources :questions
    resources :rewards
    resources :donations do
      member do
        put :new
      end
    end
    
    member do
      put :publish
    end
  end
  
  controller :statistics do
    get 'statistics/index' => :index
    get 'statistics/current_requests' => :current_requests
  end
  
  resources :prefs do
    collection do
      get :overview
    end
    
    member do
      put :switch
    end
  end
  
  resources :votes
  
  resources :comments
  
  resources :arguments
  
  resources :proposals do
    member do
      put :vote
    end
  end
  
  resources :decisions do
    resources :proposals
    
    member do
      put :publish
      put :vote
      get :result
    end
  end
  
  namespace :admin do
    resources :accounts
    
    resources :payments
  end
  
  resources :messages
  
  resources :memberships do
    member do
      put :accept
      get :activate
    end
  end
  
  resources :registrations do
    member do
      get :activate
    end
  end
  
  resources :pages
  
  resources :contentparts do
    member do
      put :up
      put :to_top
      put :down
      put :to_bottom
    end
  end
  
  resources :newsletters do
    member do
      put :ready
      post :test
    end
  end
  
  resources :articles do
    member do
      put :publish
    end
  end
  
  resources :events do
    resources :registrations
  end
  
  resources :contacts do
    member do
      get :activate
      get :unsubscribe
    end
    
    resources :messages
  end
  
  controller :sessions do
    get 'login' => :new
    post 'login'=> :create
    delete 'logout' => :destroy
  end
  
  resources :accounts do
    collection do
      match :check, via: [ :get, :post ]
      match :request_password, via: [ :get, :post ]
    end
    member do
      match :reset_password, via: [ :get, :patch ]
    end
  end
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  get '/search', to: 'application#search'
  get '/impressum', to: 'pages#impressum'
  get '/datenschutz', to: 'pages#datenschutz'
  
  # catch all route for database pages
  get '/*path', to: 'pages#show',
      constraints: lambda { |req|
          req.path.exclude? 'rails/active_storage' }
  
  root to: 'pages#root'
  
end
