# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

prefs = [
  { key: 'login_in_menu',
    description: 'Login Menu auch in der Navigation oben anzeigen',
    value: 'true', klass: 'Boolean' },
  { key: 'allow_membership',
    description: 'Mitgliedschaften',
    value: 'false', klass: 'Boolean' },
  { key: 'allow_decisions_for_team',
    description: 'Entscheidungen auch für Team Accounts statt nur für Mitglieder',
    value: 'false', klass: 'Boolean' },
  { key: 'allow_payment',
    description: 'Mitgliedsbeiträge',
    value: 'false', klass: 'Boolean' },
  { key: 'allow_funding',
    description: 'Finanzierungen',
    value: 'false', klass: 'Boolean' },
  { key: 'order_notification_mail',
    description: 'E-Mail Adresse, an die Benachrichtigungen bei eingehenden Bestellungen gesendet werden',
    value: nil, klass: 'String' },
  { key: 'contact_reply_mail',
    description: 'Antwort Adresse für Mails, die an Kontakte gesendet werden',
    value: nil, klass: 'String' },
  { key: 'twitter_handle',
    description: 'Twitter Handle (ohne @)',
    value: nil, klass: 'String' },
  { key: 'instagram_handle',
    description: 'Instagram Handle (ohne @)',
    value: nil, klass: 'String' },
  { key: 'facebook_handle',
    description: 'Facebook Handle',
    value: nil, klass: 'String' },
  { key: 'tiktok_handle',
    description: 'Tiktok Name',
    value: nil, klass: 'String' },
  { key: 'mastodon_link',
    description: 'Link zu einem Mastodon Profil',
    value: nil, klass: 'String' },
]
prefs.each do |pref|
  pref[ :level ] = 'admin'
  Pref.create pref
end
