class CreateOwners < ActiveRecord::Migration[5.2]
  def change
    create_table :owners do |t|
      t.references :account, foreign_key: true
      t.references :funding, foreign_key: true
      
      t.integer :lock_version, default: 0, null: false
      t.timestamps
    end
  end
end
