class AddRecommendationToMemberships < ActiveRecord::Migration[5.2]
  def change
    add_column :memberships, :recommendation, :string
    add_column :memberships, :accepts_rules, :boolean
  end
end
