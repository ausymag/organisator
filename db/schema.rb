# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_10_08_142414) do

  create_table "accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.string "login"
    t.string "password_digest"
    t.bigint "contact_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "token"
    t.string "label"
    t.text "description"
    t.string "displayname"
    t.string "reset_token"
    t.datetime "reset_expires_at"
    t.index ["contact_id"], name: "index_accounts_on_contact_id"
  end

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "answers", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "question_id"
    t.bigint "contact_id"
    t.string "openness"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_answers_on_contact_id"
    t.index ["question_id"], name: "index_answers_on_question_id"
  end

  create_table "arguments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.bigint "proposal_id"
    t.string "title"
    t.text "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_arguments_on_account_id"
    t.index ["proposal_id"], name: "index_arguments_on_proposal_id"
  end

  create_table "articles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.datetime "published_at"
    t.string "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "author_id"
    t.string "parent_type"
    t.bigint "parent_id"
    t.index ["author_id"], name: "index_articles_on_author_id"
    t.index ["parent_type", "parent_id"], name: "index_articles_on_parent_type_and_parent_id"
  end

  create_table "comments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.string "entity_type"
    t.bigint "entity_id"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_comments_on_account_id"
    t.index ["entity_type", "entity_id"], name: "index_comments_on_entity_type_and_entity_id"
  end

  create_table "contacts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "email"
    t.string "first_name"
    t.string "last_name"
    t.boolean "read_privacy"
    t.boolean "wants_news"
    t.datetime "news_last_sent"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "plz"
    t.boolean "offers_help"
    t.text "remarks"
    t.string "street"
    t.string "housenr"
    t.string "city"
    t.boolean "wants_form"
    t.string "phone_nr"
  end

  create_table "contentparts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "kind"
    t.integer "position"
    t.string "container_type"
    t.bigint "container_id"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["container_type", "container_id"], name: "index_contentparts_on_container_type_and_container_id"
  end

  create_table "decisions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.bigint "account_id"
    t.text "description"
    t.datetime "end_proposals_at"
    t.datetime "end_voting_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "show_results_at"
    t.string "variant"
    t.integer "vote_count"
    t.index ["account_id"], name: "index_decisions_on_account_id"
  end

  create_table "donations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "funding_id"
    t.decimal "amount", precision: 10, scale: 2
    t.bigint "contact_id"
    t.bigint "reward_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_donations_on_contact_id"
    t.index ["funding_id"], name: "index_donations_on_funding_id"
    t.index ["reward_id"], name: "index_donations_on_reward_id"
  end

  create_table "events", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "title"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "description"
    t.string "location"
    t.text "details"
    t.string "url"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "registration_starts_at"
    t.datetime "registration_ends_at"
    t.integer "max_registrations"
  end

  create_table "fundings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.string "title"
    t.string "slogan"
    t.text "short_text"
    t.integer "goal_1"
    t.integer "goal_2"
    t.integer "goal_3"
    t.string "goal_1_use"
    t.string "goal_2_use"
    t.string "goal_3_use"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.string "performance_period"
    t.text "description"
    t.string "category"
    t.string "location"
    t.string "url"
    t.string "some_1"
    t.string "some_2"
    t.string "some_3"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_fundings_on_account_id"
  end

  create_table "logs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "ip"
    t.string "actor_type"
    t.bigint "actor_id"
    t.string "action"
    t.text "comment"
    t.string "entity_type"
    t.bigint "entity_id"
    t.string "user_agent", limit: 500
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["actor_type", "actor_id"], name: "index_logs_on_actor_type_and_actor_id"
    t.index ["entity_type", "entity_id"], name: "index_logs_on_entity_type_and_entity_id"
  end

  create_table "mailings", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "contact_id"
    t.string "packet_type"
    t.bigint "packet_id"
    t.datetime "send_at"
    t.integer "clicks"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_mailings_on_contact_id"
    t.index ["packet_type", "packet_id"], name: "index_mailings_on_packet_type_and_packet_id"
  end

  create_table "memberships", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "contact_id"
    t.string "gender"
    t.date "birth_on"
    t.string "street"
    t.string "housenr"
    t.string "plz"
    t.string "city"
    t.text "other_memberships"
    t.decimal "fee_amount", precision: 10, scale: 2
    t.integer "fee_period"
    t.string "fee_mode"
    t.boolean "wants_membership"
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.datetime "fee_check_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "recommendation"
    t.boolean "accepts_rules"
    t.string "phone_nr"
    t.string "account_owner"
    t.string "iban"
    t.boolean "accepts_debit"
    t.index ["contact_id"], name: "index_memberships_on_contact_id"
  end

  create_table "messages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "sender_type"
    t.bigint "sender_id"
    t.string "receiver_type"
    t.bigint "receiver_id"
    t.text "content"
    t.datetime "read_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "subject"
    t.index ["receiver_type", "receiver_id"], name: "index_messages_on_receiver_type_and_receiver_id"
    t.index ["sender_type", "sender_id"], name: "index_messages_on_sender_type_and_sender_id"
  end

  create_table "newsletters", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.text "content"
    t.datetime "send_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orderquants", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "reward_id"
    t.integer "quantity"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["order_id"], name: "index_orderquants_on_order_id"
    t.index ["reward_id"], name: "index_orderquants_on_reward_id"
  end

  create_table "orders", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "contact_id"
    t.string "customer_kind"
    t.string "customer_location"
    t.string "delivery_org"
    t.string "delivery_name"
    t.string "delivery_street"
    t.string "delivery_housenr"
    t.string "delivery_plz"
    t.string "delivery_city"
    t.string "delivery_country"
    t.string "invoice_org"
    t.string "invoice_name"
    t.string "invoice_street"
    t.string "invoice_housenr"
    t.string "invoice_plz"
    t.string "invoice_city"
    t.string "invoice_country"
    t.boolean "read_privacy"
    t.boolean "read_terms"
    t.string "delivery_status"
    t.datetime "delivery_date_at"
    t.string "delivery_reference"
    t.string "invoice_status"
    t.datetime "invoice_date_at"
    t.string "invoice_reference"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category"
    t.boolean "invoice_differs"
    t.integer "step"
    t.index ["contact_id"], name: "index_orders_on_contact_id"
  end

  create_table "owners", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "account_id"
    t.bigint "funding_id"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_owners_on_account_id"
    t.index ["funding_id"], name: "index_owners_on_funding_id"
  end

  create_table "pages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "parent_id"
    t.string "path"
    t.string "title"
    t.string "menu"
    t.integer "position", default: 1, null: false
    t.string "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["parent_id"], name: "index_pages_on_parent_id"
  end

  create_table "payments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "membership_id"
    t.decimal "amount", precision: 10, scale: 2
    t.datetime "paid_at"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["membership_id"], name: "index_payments_on_membership_id"
  end

  create_table "prefs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.string "level"
    t.string "key"
    t.string "description"
    t.string "value"
    t.string "klass"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proposals", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "account_id"
    t.bigint "decision_id"
    t.string "title"
    t.text "description"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_proposals_on_account_id"
    t.index ["decision_id"], name: "index_proposals_on_decision_id"
  end

  create_table "questions", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.string "category"
    t.string "container_type"
    t.bigint "container_id"
    t.bigint "contact_id"
    t.string "openness"
    t.text "content"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contact_id"], name: "index_questions_on_contact_id"
    t.index ["container_type", "container_id"], name: "index_questions_on_container_type_and_container_id"
  end

  create_table "registrations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "event_id"
    t.bigint "contact_id"
    t.string "plz"
    t.string "city"
    t.string "emergency_nr"
    t.text "participants"
    t.boolean "accepts_storage"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "requests"
    t.index ["contact_id"], name: "index_registrations_on_contact_id"
    t.index ["event_id"], name: "index_registrations_on_event_id"
  end

  create_table "rewards", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "token"
    t.string "status"
    t.bigint "funding_id"
    t.decimal "price", precision: 10, scale: 2
    t.string "title"
    t.integer "available"
    t.text "description"
    t.string "delivery"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "category"
    t.index ["funding_id"], name: "index_rewards_on_funding_id"
  end

  create_table "votes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "status"
    t.bigint "account_id"
    t.bigint "proposal_id"
    t.integer "value"
    t.integer "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_votes_on_account_id"
    t.index ["proposal_id"], name: "index_votes_on_proposal_id"
  end

  add_foreign_key "accounts", "contacts"
  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "answers", "contacts"
  add_foreign_key "answers", "questions"
  add_foreign_key "arguments", "accounts"
  add_foreign_key "arguments", "proposals"
  add_foreign_key "articles", "accounts", column: "author_id"
  add_foreign_key "comments", "accounts"
  add_foreign_key "decisions", "accounts"
  add_foreign_key "donations", "contacts"
  add_foreign_key "donations", "fundings"
  add_foreign_key "donations", "rewards"
  add_foreign_key "mailings", "contacts"
  add_foreign_key "memberships", "contacts"
  add_foreign_key "orderquants", "orders"
  add_foreign_key "orderquants", "rewards"
  add_foreign_key "orders", "contacts"
  add_foreign_key "owners", "accounts"
  add_foreign_key "owners", "fundings"
  add_foreign_key "pages", "pages", column: "parent_id"
  add_foreign_key "payments", "memberships"
  add_foreign_key "proposals", "accounts"
  add_foreign_key "proposals", "decisions"
  add_foreign_key "questions", "contacts"
  add_foreign_key "registrations", "contacts"
  add_foreign_key "registrations", "events"
  add_foreign_key "rewards", "fundings"
  add_foreign_key "votes", "accounts"
  add_foreign_key "votes", "proposals"
end
